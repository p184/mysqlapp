from app import app
from app.controllers import borrow_controller
from flask import Blueprint, request

borrows_blueprint = Blueprint("borrows_router", __name__)

@app.route("/borrows")
def showBorrow():
    return borrow_controller.shows()

@app.route("/borrows/insert")
def insertBorrow():
    params = request.json
    return borrow_controller.insert(**params)

@app.route("/borrows/status")
def updateStatus():
    params = request.json
    return borrow_controller.changeStatus(**params)