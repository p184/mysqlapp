from app import app
from app.controllers import customers_controller
from flask import Blueprint, request

customers_blueprint = Blueprint("customers_router", __name__)

@app.route("/users")
def showUsers():
    return customers_controller.shows()

@app.route("/user", methods=["POST"])
def showUser():
    params = request.json
    return customers_controller.show(**params)

@app.route("/user/insert")
def insertUser():
    params = request.json
    return customers_controller.insert(**params)

@app.route("/user/update")
def updateUser():
    params = request.json
    return customers_controller.update(**params)

@app.route("/user/delete")
def deleteUser():
    params = request.json
    return customers_controller.delete(**params)

@app.route("/user/requesttoken")
def requestToken():
    params = request.json
    return customers_controller.token(**params)
