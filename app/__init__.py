from flask import Flask
#from config import Config
import os

base_dir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    JSON_SORT_KEYS = False
    JWT_SECRET_KEY = str(os.environ.get("JWT_SECRET"))
    
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config.from_object(Config)
jwt = JWTManager(app)

from app.routers.customers_router import *
from app.routers.borrows_router import *

app.register_blueprint(customers_blueprint)
app.register_blueprint(borrows_blueprint)