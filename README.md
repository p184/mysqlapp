# mysqlapp

mysqlapp digunakan untuk membangun aplikasi untuk customers yang menangani biodata user dan aplikasi borrows untuk peminjaman buku. 

Pada aplikasi customers terdapat beberapa fungsi yaitu:
- melihat user
    untuk melihat semua user, gunakan REST API localhost:5000/users
- melihat user berdasarkan id
    untuk melihat user berdasarkan idnya, gunakan REST API localhost:5000/user dan pada bagian Body, pilih raw, lalu pilih file JSON dan tuliskan { "userid":[id dari user yang ingin ditampilkan]}
- memasukkan data user baru
    untuk menambah data user baru, gunakan REST API localhost:5000/user/insert dan pada bagian Body, pilih raw, lalu pilih file JSON dan tuliskan data yang ingin ditambahkan
- mengubah data user
    untuk mengubah data user, gunakan REST API localhost:5000/user/update dan pada bagian Body, pilih raw, lalu pilih file JSON dan tuliskan id user yang ingin diubah dan data baru yang ingin diubah 
- menghapus data user
    untuk menghapus data user, gunakan REST API localhost:5000/user/delete dan pada bagian Body, pilih raw, lalu pilih file JSON dan tuliskan id user yang ingin dihapus
    
Pada aplikasi borrows terdapat beberapa fungsi yaitu:
- melihat daftar peminjaman untuk userid tertentu
- menambahkan daftar pinjaman
- mengubah status pinjaman